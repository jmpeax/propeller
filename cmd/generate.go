/*
Copyright © 2020 Propeller

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"github.com/spf13/cobra"
	"log"
	y "propeller/yaml"
)

/**
  propeller:
	terraform:
		- name: infra
          source: http://myurl.com
          vars:
			- name: AB
              value: XY
          varFile: ./something.tf | http://myurl.com/something.tf
*/

var OutputFile string

var generateCmd = &cobra.Command{
	Use:   "generate",
	Short: "Generate a Propeller template configuration file",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		templateStr := &y.Propeller{
			Terraform: []y.Terraform{{
				Name:   "Sample",
				Source: "source",
				Vars: []y.TerraformVar{{
					Name:  "var",
					Value: "val",
				}},
				VarFile: "file",
			}},
		}
		y.WriteFile(templateStr, OutputFile)
		log.Printf("File %s was succesfully generated.", OutputFile)
	},
}

func init() {
	rootCmd.AddCommand(generateCmd)
	generateCmd.Flags().StringVarP(&OutputFile, "output", "o", "propeller.yaml", "Propeller output file")
}
