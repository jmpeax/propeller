package cmd

import (
	"github.com/spf13/cobra"
	"log"
	"os"
	gitUtils "propeller/utils"
	y "propeller/yaml"
)

/**
 init -f properller.yaml
**/

var InputFile string
var WorkingDirectory string
var initCmd = &cobra.Command{
	Use:   "init",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		propeller := y.ReadFile(InputFile)
		if verbose {
			log.Printf("Readed File %s", propeller)
		}
		for _, value := range propeller.Terraform {
			gitUtils.Clone(value.Source, WorkingDirectory, value.Name)
		}
	},
}

func wgd() string {
	dir, err := os.Getwd()
	if err != nil {
		log.Fatalf("Unable to get CWD, %s", err)
	}
	return dir
}
func init() {
	rootCmd.AddCommand(initCmd)
	initCmd.Flags().StringVarP(&InputFile, "file", "f", "propeller.yaml", "Propeller File.")
	initCmd.Flags().StringVarP(&WorkingDirectory, "workingdir", "w", wgd(), "Working Directory")
}
