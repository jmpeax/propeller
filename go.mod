module propeller

go 1.14

require (
	github.com/go-git/go-git/v5 v5.1.0
	github.com/mitchellh/go-homedir v1.1.0
	github.com/spf13/cobra v1.0.0
	github.com/spf13/viper v1.4.0
	gopkg.in/yaml.v2 v2.2.4
)
