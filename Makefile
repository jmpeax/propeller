# Go parameters
GOCMD=go
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
GOGET=$(GOCMD) get
BINARY_NAME=propeller

all: test build
build:
		$(GOBUILD) -o $(BINARY_NAME) -v
test:
		$(GOTEST) -v ./...
clean:
		$(GOCLEAN)
		rm -f $(BINARY_NAME)
		rm -f $(BINARY_UNIX)
run:
		$(GOBUILD) -o $(BINARY_NAME) -v ./...
		./$(BINARY_NAME)
deps:
		$(GOGET) github.com/markbates/goth
		$(GOGET) github.com/markbates/pop
full: build-freebsd build-linux build-openbsd build-osx build-win64
# Cross compilation
build-linux:
		CGO_ENABLED=0 GOOS=linux GOARCH=amd64 $(GOBUILD)  -v -o build/linux/propeller
build-win64:
		CGO_ENABLED=0 GOOS=windows GOARCH=amd64 $(GOBUILD)  -v -o build/win64/propeller.exe
build-osx:
		CGO_ENABLED=0 GOOS=darwin GOARCH=amd64 $(GOBUILD)  -v -o build/osx/propeller
build-openbsd:
		CGO_ENABLED=0 GOOS=darwin GOARCH=amd64 $(GOBUILD)  -v -o build/openbsd/propeller
build-freebsd:
		CGO_ENABLED=0 GOOS=darwin GOARCH=amd64 $(GOBUILD)  -v -o build/freebsd/propeller
