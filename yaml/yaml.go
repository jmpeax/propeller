package yaml

import (
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"os"
)

type TerraformVar struct {
	Name  string `yaml:"name"`
	Value string `yaml:"value"`
}
type Terraform struct {
	Name    string `yaml:"name"`
	Source  string `yaml:"source"`
	Vars    []TerraformVar
	VarFile string `yaml:"varFile"`
}

type Propeller struct {
	Terraform []Terraform `yaml:"terraform"`
}

func ReadFile(inputFile string) *Propeller {
	propeller := &Propeller{}
	file, err := ioutil.ReadFile(inputFile)
	if err != nil {
		log.Fatalf("Unable to read file %s", err)
	}
	unmarshalError := yaml.Unmarshal(file, propeller)
	if unmarshalError != nil {
		log.Fatalf("Unable to unmarshal the given file %s, %s", inputFile, unmarshalError)
	}
	return propeller
}

func WriteFile(propeller *Propeller, output string) {
	templateBytes, err := yaml.Marshal(propeller)
	if err != nil {
		log.Fatalf(" Error %v", err)
	}
	f, ioErr := os.Create(output)
	if ioErr != nil {
		log.Fatalf("Error %v", ioErr)
	}
	_, writeError := f.Write(templateBytes)
	if writeError != nil {
		log.Fatalf("Unable to write yaml file %s, %s ", output, writeError)
	}
	syncError := f.Sync()
	if syncError != nil {
		log.Fatalf("Unable to write yaml file %s, %s ", output, syncError)
	}
	closeError := f.Close()

	if closeError != nil {
		log.Fatalf("Unable to write yaml file %s, %s ", output, closeError)
	}
}
