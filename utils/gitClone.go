package gitUtils

import (
	"github.com/go-git/go-git/v5"
	"log"
	"os"
	"path"
)

func Clone(repo string, wd string, name string) {
	var fullPath = path.Join(wd, name)
	if FolderExists(fullPath) {
		deleteErr := os.RemoveAll(fullPath)
		if deleteErr != nil {
			log.Fatalf("Unable to delete %s, %s", fullPath, deleteErr)
		}
	}
	_, err := git.PlainClone(fullPath, false, &git.CloneOptions{URL: repo, Progress: os.Stdout})
	if err != nil {
		log.Fatalf("Unable to Clone %s", err)
	}
}

func FolderExists(path string) bool {
	_, err := os.Stat(path)
	return !os.IsNotExist(err)
}
